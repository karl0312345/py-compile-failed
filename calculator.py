"""Calculator module"""
class Calculator:
    """calculator class"""

ddef __init__(self, x_1, x_2):
        """Init"""
        self.x_1 = x_1
        self.x_2 = x_2

    def add(self):
        """add method"""
        return self.x_1 + self.x_2

    def multiply(self):
        """multiply method"""
        return self.x_1 * self.x_2

    def subtract(self):
        """subtract method"""
        return self.x_1 - self.x_2

    def divide(self):
        """divide method"""
        return self.x_1 / self.x_2
